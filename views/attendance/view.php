<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Attendance */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Attendances', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attendance-view">

    <h1>Attendance: <?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'label' => 'Pupil',
                'value' => $model->pupil->fullName,
            ],
            [
                'label' => 'Grade',
                'value' => $model->grade->name,
            ],
            [
                'attribute' => 'file',
                'value' => !empty($model->file) ? Html::a('Download', ['attendance/download', 'id' => $model->id]) : null,
                'format' => 'html',
            ],
            [
                'label' => 'Absent',
                'value' => $model->absentText,
            ],
            'absent_reason',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
