<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use app\models\Grade;
use app\models\Pupil;

/* @var $this yii\web\View */
/* @var $model app\models\Attendance */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="attendance-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
        echo $form->field($model, 'pupil_id')
            ->dropdownList(
                Pupil::getPupilList(),
                ['prompt' => 'Select Pupil']
            );
    ?>

    <?php
        echo $form->field($model, 'grade_id')
            ->dropdownList(
                Grade::getGradeList(),
                ['prompt' => 'Select Grade']
            );
    ?>

    <?= $form->field($model, 'absent')->checkbox() ?>

    <?= $form->field($model, 'absent_reason')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'file')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
