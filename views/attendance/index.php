<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AttendanceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Attendances';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attendance-index">

    <h1><?= date('d M Y'); ?></h1>

    <p>
        <?= Html::a('Create Attendance', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'label' => 'Pupil',
                'attribute' => 'pupil',
                'value' => 'pupil.fullName',
            ],
            [
                'label' => 'Grade',
                'attribute' => 'grade',
                'value' => 'grade.name',
            ],
            [
                'attribute' => 'absent',
                'value' => 'absentText',
                'filter' => [0 => 'No', 1 => 'Yes'],
            ],
            'absent_reason',
            [
                'attribute' => 'file',
                'value' => function ($data) {
                    return !empty($data->file) ? Html::a('Download', ['attendance/download', 'id' => $data->id]) : null;
                },
                'format' => 'html',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
