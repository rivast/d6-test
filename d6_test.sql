SET NAMES utf8;
SET time_zone = '+02:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `attendance`;
CREATE TABLE `attendance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pupil_id` int(11) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `absent` tinyint(1) NOT NULL DEFAULT '0',
  `absent_reason` varchar(500) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_attendance_pupil1` (`pupil_id`),
  KEY `fk_attendance_grade1` (`grade_id`),
  CONSTRAINT `fk_attendance_grade1` FOREIGN KEY (`grade_id`) REFERENCES `grade` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_attendance_pupil1` FOREIGN KEY (`pupil_id`) REFERENCES `pupil` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `grade`;
CREATE TABLE `grade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `grade` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1,	'Grade 1',	'2016-08-30 04:30:26',	'2016-08-30 04:30:26'),
(2,	'Grade 2',	'2016-08-30 04:30:26',	'2016-08-30 04:30:26'),
(3,	'Grade 3',	'2016-08-30 04:30:26',	'2016-08-30 04:30:26'),
(4,	'Grade 4',	'2016-08-30 04:30:26',	'2016-08-30 04:30:26'),
(5,	'Grade 5',	'2016-08-30 04:30:26',	'2016-08-30 04:30:26'),
(6,	'Grade 6',	'2016-08-30 04:30:26',	'2016-08-30 04:30:26'),
(7,	'Grade 7',	'2016-08-30 04:30:26',	'2016-08-30 04:30:26');

DROP TABLE IF EXISTS `pupil`;
CREATE TABLE `pupil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `pupil` (`id`, `name`, `surname`, `created_at`, `updated_at`) VALUES
(1,	'James',	'Smith',	'2016-08-30 04:30:26',	'2016-08-30 04:30:26'),
(2,	'Paul',	'Harris',	'2016-08-30 04:30:26',	'2016-08-30 04:30:26'),
(3,	'Michelle',	'Finley',	'2016-08-30 04:30:26',	'2016-08-30 04:30:26'),
(4,	'David',	'Gates',	'2016-08-30 04:30:26',	'2016-08-30 04:30:26'),
(5,	'Angela',	'Nelson',	'2016-08-30 04:30:26',	'2016-08-30 04:30:26');
