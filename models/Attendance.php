<?php

namespace app\models;

use Yii;

use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;

use mongosoft\file\UploadBehavior;

/**
 * This is the model class for table "attendance".
 *
 * @property integer $id
 * @property integer $pupil_id
 * @property integer $grade_id
 * @property integer $absent
 * @property string $absent_reason
 * @property string $file
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Grade $grade
 * @property Pupil $pupil
 */
class Attendance extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attendance';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'file',
                'scenarios' => ['default'],
                'path' => '@webroot/upload',
                'url' => '@web/upload',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pupil_id', 'grade_id'], 'required'],
            [['pupil_id', 'grade_id', 'absent'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['absent_reason'], 'string', 'max' => 500],
            [['grade_id'], 'exist', 'skipOnError' => true, 'targetClass' => Grade::className(), 'targetAttribute' => ['grade_id' => 'id']],
            [['pupil_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pupil::className(), 'targetAttribute' => ['pupil_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pupil_id' => 'Pupil',
            'grade_id' => 'Grade',
            'file' => 'Sick Letter',
            'absent' => 'Absent',
            'absent_reason' => 'Reason for absence',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrade()
    {
        return $this->hasOne(Grade::className(), ['id' => 'grade_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPupil()
    {
        return $this->hasOne(Pupil::className(), ['id' => 'pupil_id']);
    }

    /**
     * Returns Yes or No
     * @return string
     */
    public function getAbsentText()
    {
        if($this->absent)
            return 'Yes';
        else
            return 'No';
    }
}
