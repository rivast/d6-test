<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Attendance;

/**
 * AttendanceSearch represents the model behind the search form about `app\models\Attendance`.
 */
class AttendanceSearch extends Attendance
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'absent'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Attendance::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // Only show today's entries
        $query->andWhere(['>=', 'created_at', date('Y-m-d')]);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'absent' => $this->absent,
        ]);

        // $query->andFilterWhere(['like', 'grade.name', $this->grade]);
        // $query->andFilterWhere(['like', 'pupil.fullName', $this->pupil]);

        return $dataProvider;
    }
}
