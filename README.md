# School Attendance Register

A school attendance register for capturing the attendance of pupils on a daily basis.

## Installation
1. Create a new test database
    i.e. `CREATE DATABASE d6_test`;
2. Do a fork / checkout of this repo
3. Configure the database connection in `config/db.php`
4. Run the Yii migration script from the command line to create the necessary tables
    i.e. `php yii migrate`

Optional:
  Browse to the `requirements.php` file for compatibility checks
  
FYI: All the necessary database tables are available in the `d6_test.sql` file