<?php

use yii\db\Migration;

/**
 * Handles the creation for table `grade`.
 */
class m160829_193045_create_grade_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('grade', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        for ($i=1; $i <= 7; $i++) {
            $data[] = ['Grade '.$i, date('Y-m-d H:i:s'), date('Y-m-d H:i:s')];
        }

        $this->batchInsert(
            'grade',
            ['name', 'created_at', 'updated_at'],
            $data
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('grade');
    }
}
