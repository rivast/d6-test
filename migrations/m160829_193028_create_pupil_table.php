<?php

use yii\db\Migration;

/**
 * Handles the creation for table `pupil`.
 */
class m160829_193028_create_pupil_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('pupil', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'surname' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        $pupils = [
            ['James', 'Smith'],
            ['Paul', 'Harris'],
            ['Michelle', 'Finley'],
            ['David', 'Gates'],
            ['Angela', 'Nelson'],
        ];

        $data = [];
        foreach ($pupils as $pupil) {
            $data[] = array_merge($pupil, [date('Y-m-d H:i:s'), date('Y-m-d H:i:s')]);
        }

        $this->batchInsert(
            'pupil',
            ['name', 'surname', 'created_at', 'updated_at'],
            $data
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('pupil');
    }
}
