<?php

use yii\db\Migration;

/**
 * Handles the creation for table `attendance`.
 */
class m160829_193102_create_attendance_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('attendance', [
            'id' => $this->primaryKey(),
            'pupil_id' => $this->integer()->notNull(),
            'grade_id' => $this->integer()->notNull(),
            'absent' => $this->boolean()->notNull()->defaultValue(0),
            'absent_reason' => $this->string(500),
            'file' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        // add foreign key for table `pupil`
        $this->addForeignKey(
            'fk_attendance_pupil1',
            'attendance',
            'pupil_id',
            'pupil',
            'id',
            'NO ACTION',
            'NO ACTION'
        );

        // add foreign key for table `grade`
        $this->addForeignKey(
            'fk_attendance_grade1',
            'attendance',
            'grade_id',
            'grade',
            'id',
            'NO ACTION',
            'NO ACTION'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `pupil`
        $this->dropForeignKey(
            'fk_attendance_pupil1',
            'attendance'
        );

        // drops foreign key for table `grade`
        $this->dropForeignKey(
            'fk_attendance_grade1',
            'attendance'
        );

        $this->dropTable('attendance');
    }
}
